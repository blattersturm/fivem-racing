--resource_type 'gametype' { name = 'Racing' }
-- Manifest
resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Racing gamemode by mraes'
client_script 'client/zzdebug.lua'
client_script 'client/xml.lua'
client_script 'client/map_loader.lua'
client_script 'client/gfx.lua'

client_script 'client/racing_client.lua'

--server_script 'server/lib/NewtonSoft.Json.net.dll'
server_script 'server/RaceServer.net.dll'
